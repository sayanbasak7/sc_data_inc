#!/usr/bin/env python
# coding: utf-8
#
# Using bitbucket : https://bitbucket.org/sayanbasak7/sc_data_inc/src/master

# # Searching for chemical composition of Superconductors

import pandas as pd
import numpy as np
from scipy import stats
import time
import matplotlib.pyplot as plt

# ### Superconductor data 

data = pd.read_csv("Supercon_data_2.csv")


# #### Separate elements and their stoichiometric weights into columns

comp = list(data["name"])

comp2str = []
comp2wt = []
for s in comp:
    temp2str = []
    temp2beg = []
    temp2end = []
    temp2wt = []
    i = 0
    while (i<len(s)):
        if ((s[i]).isalpha()):
            try:
                if ((s[i+1]).islower()):
                    temp2str.append(s[i:i+2])
                    if (i>0):
                        temp2end.append(i)
                    i += 2
                    temp2beg.append(i)
                else:
                    temp2str.append(s[i:i+1])
                    if (i>0):
                        temp2end.append(i)
                    i += 1
                    temp2beg.append(i)
            except:
                temp2str.append(s[i:i+1])
                if (i>0):
                    temp2end.append(i)
                i += 1
                temp2end.append(i)
        else:
            i += 1
    temp2end.append(len(s))
    comp2str.append(temp2str)
    max
    for i,j in zip(temp2beg,temp2end):
        if (i==j):
            temp2wt.append(1)
        else:
            temp2wt.append(float(s[i:j]))
            if (float(s[i:j])>100):
                print("stochiometric weight > 100 :",temp2str, temp2wt, s)
    comp2wt.append(temp2wt)


# #### Rename column header because we will use Tc for Technetium

data.rename(columns={'Tc' : 'Critical Temperature',
                    'name' : 'Compound'},
           inplace=True)


# #### Drop compounds with zero Critical Temperature

indexNames = data[ data['Critical Temperature'] == 0 ].index

data.drop(indexNames , inplace=True)

print("remaining data points :", data.shape[0])

# #### Set the stoichiometric coefficients

for symbols in comp2str:
    for symbol in symbols:
        data[symbol] = 0

for row,symbols,weights in zip(data.index,comp2str,comp2wt):
    for symbol,weight in zip(symbols,weights):
        try:
            data.at[row,symbol] = weight
        except:
            print(row,symbol,weight)
            print(data.at[row,"Compound"])
            

# ### Basic element properties

elements = pd.read_csv("elements_data_2.csv")

# #### Set unknown electronegativity
# * Electronnegativity of Deuterium and Tritium set same as Hydrogen
# * Unknown Noble gas electronegativity set to 0
# * Elements with Z>106 set to zero as they are irrelevant

indexNames = list(elements[elements["Atomic number"] == 1].index)
indexH = int(elements[elements["Symbol"].isin(["H"])].index[0])
elements.loc[indexNames,"Electro-negativity"] = elements.at[indexH,"Electro-negativity"]

indexNames = list(elements[elements["Electro-negativity"].isin(["NaN"])].index)
elements.loc[indexNames,"Electro-negativity"] = 0


# #### Construct features using atomic number(Z), atomic mass(m_a), electronegativity(X) and stoichiometric coefficient

for symbols in comp2str:
    for symbol in symbols:
        data["Z_"+symbol] = 0
        data["m_a_"+symbol] = 0
        data["X_"+symbol] = 0
    
for row,symbols,weights in zip(data.index,comp2str,comp2wt):
    for symbol,weight in zip(symbols,weights):
            indexElem = elements[elements["Symbol"].isin([symbol])].index[0]
            
            data.at[row,"Z_"+symbol] = weight*elements.at[indexElem, "Atomic number"]
            data.at[row,"m_a_"+symbol] = weight*elements.at[indexElem, "Atomic weight (u)"]
            data.at[row,"X_"+symbol] = weight*elements.at[indexElem, "Electro-negativity"]


# ### Check for correlation of the cumulative features

symbols = list(data.head(0))[2:2+sym_len]

data["Cumulative Z"] = 0
data["Cumulative m_a"] = 0
data["Cumulative X"] = 0

for row,symbols,weights in zip(data.index,comp2str,comp2wt):
    for symbol,weight in zip(symbols,weights):
            data.at[row,"Cumulative Z"] += data.at[row,"Z_"+symbol]
            data.at[row,"Cumulative m_a"] += data.at[row,"m_a_"+symbol]
            data.at[row,"Cumulative X"] += data.at[row,"X_"+symbol]


data_Tc = data["Critical Temperature"]
data_Z = data["Cumulative Z"]
data_m_a = data["Cumulative m_a"]
data_X = data["Cumulative X"]

covariance_Z_Tc = np.cov(data_Z,data_Tc)
pearson_corr_Z_Tc, _ = stats.pearsonr(data_Z,data_Tc)
spearman_corr_Z_Tc, _ = stats.spearmanr(data_Z,data_Tc)

covariance_m_a_Tc = np.cov(data_m_a,data_Tc)
pearson_corr_m_a_Tc, _ = stats.pearsonr(data_m_a,data_Tc)
spearman_corr_m_a_Tc, _ = stats.spearmanr(data_m_a,data_Tc)

covariance_X_Tc = np.cov(data_X,data_Tc)
pearson_corr_X_Tc, _ = stats.pearsonr(data_X,data_Tc)
spearman_corr_X_Tc, _ = stats.spearmanr(data_X,data_Tc)

covariance_Z_m_a = np.cov(data_Z,data_m_a)
pearson_corr_Z_m_a, _ = stats.pearsonr(data_Z,data_m_a)
spearman_corr_Z_m_a, _ = stats.spearmanr(data_Z,data_m_a)

covariance_m_a_X = np.cov(data_m_a,data_X)
pearson_corr_m_a_X, _ = stats.pearsonr(data_m_a,data_X)
spearman_corr_m_a_X, _ = stats.spearmanr(data_m_a,data_X)

covariance_X_Z = np.cov(data_X,data_Z)
pearson_corr_X_Z, _ = stats.pearsonr(data_X,data_Z)
spearman_corr_X_Z, _ = stats.spearmanr(data_X,data_Z)

print("covariance (Z,Tc) = ", covariance_Z_Tc.ravel())
print("pearson correlation (Z,Tc) = ", pearson_corr_Z_Tc)
print("spearman correlation (Z,Tc) = ",spearman_corr_Z_Tc)

print("covariance (m_a,Tc) = ", covariance_m_a_Tc.ravel())
print("pearson correlation (m_a,Tc) = ", pearson_corr_m_a_Tc)
print("spearman correlation (m_a,Tc) = ",spearman_corr_m_a_Tc)

print("covariance (X,Tc) = ", covariance_X_Tc.ravel())
print("pearson correlation (X,Tc) = ", pearson_corr_X_Tc)
print("spearman correlation (X,Tc) = ",spearman_corr_X_Tc)

print("covariance (Z,m_a) = ", covariance_Z_m_a.ravel())
print("pearson correlation (Z,m_a) = ", pearson_corr_Z_m_a)
print("spearman correlation (Z,m_a) = ",spearman_corr_Z_m_a)

print("covariance (m_a,X) = ", covariance_m_a_X.ravel())
print("pearson correlation (m_a,X) = ", pearson_corr_m_a_X)
print("spearman correlation (m_a,X) = ",spearman_corr_m_a_X)

print("covariance (X,Z) = ", covariance_X_Z.ravel())
print("pearson correlation (X,Z) = ", pearson_corr_X_Z)
print("spearman correlation (X,Z) = ",spearman_corr_X_Z)

plt.figure(figsize=(12,10))
plt.xlim(0,145)
plt.ylim(0,50)
plt.xlabel("Critical Temperature")
plt.ylabel("Cumulative Electronegativity")
plt.scatter(data_Tc,data_X, s=5, c=(data_Z))
plt.colorbar()
plt.show()




sorted_Tc = np.argsort(data_Tc)[::-1]
plt.figure(figsize=(12,10))
# plt.xlim(0,100)
plt.ylim(0,20000)
plt.xlabel("Cumulative Electronegativity")
plt.ylabel("Molecular Mass")
plt.scatter(data_X[sorted_Tc], data_m_a[sorted_Tc], s=10*np.floor(data_Tc[sorted_Tc]/10), c=np.floor(data_Tc[sorted_Tc]/50), alpha=0.7)
plt.colorbar()
plt.show()


