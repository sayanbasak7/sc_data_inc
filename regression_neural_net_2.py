#!/usr/bin/env python
# coding: utf-8

# ## Searching for a regression model for Chemical Composition

from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Flatten, Dropout, Multiply
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error 

import warnings 
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)
import numpy as np
import pandas as pd
import time
import matplotlib.pyplot as plt
import seaborn as sb

from jupyterthemes import jtplot
jtplot.style(theme='monokai', context='notebook', ticks=True, grid=True)

data = pd.read_csv("SC_data_frac.csv") 

# #### Make custom training, validation and test set

elements = pd.read_csv("elements_data_2.csv")
indexNames = list(elements[elements["Atomic number"] == 1].index)
indexH = int(elements[elements["Symbol"].isin(["H"])].index[0])
elements.loc[indexNames,"Electro-negativity"] = elements.at[indexH,"Electro-negativity"]

indexNames = list(elements[elements["Electro-negativity"].isin(["NaN"])].index)
elements.loc[indexNames,"Electro-negativity"] = 0.0

colNames = [col[:-2] for col in data if col.endswith('_x')]

data["Cumulative Z"] = 0.0
data["Cumulative m_a"] = 0.0
data["Cumulative X"] = 0.0
data["Average Z"] = 0.0
data["Average m_a"] = 0.0
data["Average X"] = 0.0
data["Number of atoms"] = 0.0
for col in colNames:
    indexElem = elements[elements["Symbol"].isin([col])].index[0]
    data["Number of atoms"] += data[col+"_x"]
    data["Cumulative Z"] += data[col+"_x"] * float(elements.at[indexElem, "Atomic number"])
    data["Cumulative m_a"] += data[col+"_x"] * float(elements.at[indexElem, "Atomic weight (u)"])
    data["Cumulative X"] += data[col+"_x"] * float(elements.at[indexElem, "Electro-negativity"])

data["Average Z"] = data["Cumulative Z"]/ data["Number of atoms"]
data["Average m_a"] = data["Cumulative m_a"]/ data["Number of atoms"]
data["Average X"] = data["Cumulative X"]/ data["Number of atoms"]

colNames = [col[:-2] for col in data if col.endswith('_x')]

data["vec_norm"] = 0.0
data["Scaled T_c"] = data["Critical Temperature"]/data["Critical Temperature"].std()

for col in colNames:
    data[col+"_bool"] = np.array(data[col+"_x"]>0,dtype=int)
    data["vec_norm"] += np.array(data[col+"_x"])**2
data["vec_norm"] = np.array(data["vec_norm"])**(1/2)

for col in colNames:
    data[col+"_vec"] = np.array(data[col+"_x"])/data["vec_norm"]

for col in colNames:
    data[col+"_x_s"] = data[col+"_x"]/data[col+"_x"].std()

val_ = data.head(0)
train_ = data.head(0)
test_ = data.head(0)

data_0 = data[data["Critical Temperature"]>0].reset_index(drop=True)
frac_01 = int(len(data_0)*0.1)
val_ = val_.append(data_0.loc[0:frac_01,:])
train_ = train_.append(data_0.loc[frac_01:8*frac_01,:])
val_ = val_.append(data_0.loc[8*frac_01:9*frac_01,:])
test_ = test_.append(data_0.loc[9*frac_01:len(data_0),:])

data_std = data_0.std()

val_X = val_.loc[:,[col for col in data_0 if col.endswith('_bool') or col.endswith('Scaled T_c')]]
test_X = test_.loc[:,[col for col in data_0 if col.endswith('_bool') or col.endswith('Scaled T_c')]]
train_X = train_.loc[:,[col for col in data_0 if col.endswith('_bool') or col.endswith('Scaled T_c')]]

val_y = val_.loc[:,[col for col in data_0 if col.endswith('_p')]]
test_y = test_.loc[:,[col for col in data_0 if col.endswith('_p')]]
train_y = train_.loc[:,[col for col in data_0 if col.endswith('_p')]]

data_std["Critical Temperature"]

# #### Make a Deep Neural Network 

NN_model = Sequential()

NN_model.add(Dense(256, kernel_initializer='normal', input_dim = train_X.shape[1], activation='relu'))
NN_model.add(Dense(256, kernel_initializer='normal',activation='relu'))
NN_model.add(Dense(256, kernel_initializer='normal',activation='sigmoid'))

NN_model.add(Dense(train_y.shape[1], kernel_initializer='normal'))

NN_model.compile(loss='mean_absolute_error', optimizer='adam', metrics=['mean_absolute_error'])
NN_model.summary()

checkpoint_name = 'Weights2-{epoch:03d}--{val_loss:.5f}.hdf5' 
checkpoint = ModelCheckpoint(checkpoint_name, monitor='val_loss', verbose = 1, save_best_only = True, mode ='auto')
callbacks_list = [checkpoint]

NN_model.fit(train_X, train_y, epochs=100, batch_size=32, validation_data = (val_X, val_y), callbacks=callbacks_list)

# Load weights file of the best model :
weights_file = 'Weights-248--0.00420.hdf5' # choose the best checkpoint 
NN_model.load_weights(weights_file) # load it
NN_model.compile(loss='mean_absolute_error', optimizer='adam', metrics=['mean_absolute_error'])

Y_train_pred = NN_model.predict(train_X)
Y_val_pred = NN_model.predict(val_X)
Y_test_pred = NN_model.predict(test_X)
fig = plt.figure( figsize = (10,10) )
ax = fig.add_subplot(1,1,1)
sym=range(87)
plt.scatter(np.array(train_y)[sym].flatten(),np.array(Y_train_pred)[sym].flatten(), label="train-"+train_y.columns.values[sym][:-4])#, s=1000*np.abs(np.array(train_y)[sym].flatten()-np.array(Y_train_pred)[sym].flatten())+1)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
plt.scatter(np.array(val_y)[sym].flatten(),np.array(Y_val_pred)[sym].flatten(), label="val-"+val_y.columns.values[sym][:-4])#, s=1000*np.abs(np.array(val_y)[sym].flatten()-np.array(Y_val_pred)[sym].flatten())+1)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
plt.scatter(np.array(test_y)[sym].flatten(),np.array(Y_test_pred)[sym].flatten(), label="test-"+test_y.columns.values[sym][:-4])#, s=1000*np.abs(np.array(test_y)[sym].flatten()-np.array(Y_test_pred)[sym].flatten())+1)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
plt.show()

np.min(np.sum(Y_test_pred,axis=1))

# ### Try Random Forest Regressor

model = RandomForestRegressor()
model.fit(train_X.append(val_X),train_y.append(val_y))

predicted_Tc = model.predict(test_X)
MAE = mean_absolute_error(test_y , predicted_Tc)
print('Random forest validation MAE = ', MAE)

Y_train_pred = model.predict(train_X)
Y_val_pred = model.predict(val_X)
Y_test_pred = model.predict(test_X)
fig = plt.figure( figsize = (10,10) )
ax = fig.add_subplot(1,1,1)
sym=range(87)
plt.scatter(np.array(train_y)[sym].flatten(),np.array(Y_train_pred)[sym].flatten(), label="-training-")#+train_y.columns.values[sym][:-4])#, s=1000*np.abs(np.array(train_y)[sym].flatten()-np.array(Y_train_pred)[sym].flatten())+1)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
plt.scatter(np.array(val_y)[sym].flatten(),np.array(Y_val_pred)[sym].flatten(), label="-validation-")#+val_y.columns.values[sym][:-4])#, s=1000*np.abs(np.array(val_y)[sym].flatten()-np.array(Y_val_pred)[sym].flatten())+1)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
plt.scatter(np.array(test_y)[sym].flatten(),np.array(Y_test_pred)[sym].flatten(), label="-test-")#+test_y.columns.values[sym][:-4])#, s=1000*np.abs(np.array(test_y)[sym].flatten()-np.array(Y_test_pred)[sym].flatten())+1)
ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
ax.set_xlabel("Actual proportion of element")
ax.set_ylabel("Predicted proportion of element")
plt.show()
plt.savefig("plot4.pdf")

test_X_200plus = test_X.head(0)

for i,t in enumerate(range(50,150,1)):
    test_X_200plus.loc[i,"Scaled T_c"] = t/data_std["Critical Temperature"]
    test_X_200plus.loc[i,[col for col in data if col.endswith("_bool") ]] = 1

Y_200plus_pred = model.predict(test_X_200plus)
fig = plt.figure(figsize=(10,10))
for i,sym in enumerate([col for col in data if col.endswith("_bool")]):
    plt.plot(data_std["Critical Temperature"]*test_X_200plus["Scaled T_c"],Y_200plus_pred[:,i],label=sym[:-5]+"-test",linestyle='--')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.06), fancybox=True, framealpha=0.1, ncol=10)
plt.ylabel("Predicted proportion of elements")
plt.xlabel("Temperature")
plt.show()
plt.savefig("plot5.pdf")
