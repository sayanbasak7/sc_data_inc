# Supercon
Data used in "[Machine learning modeling of superconducting critical temperature](https://www.nature.com/articles/s41524-018-0085-8)" paper.
Downloaded from [SuperCon database](https://github.com/vstanev1/Supercon).

# Elements
Data is imported from wikipedia article : [List of chemical elements](https://en.wikipedia.org/wiki/List_of_chemical_elements)
electronic configuration is imported from : [Electron configurations of the elements (data page)](https://en.wikipedia.org/wiki/Electron_configurations_of_the_elements_(data_page))

# Launch interactive notebook in binder
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fsayanbasak7%2Fsc_data_inc/master?filepath=interactive_regressor.ipynb)