valid_elems = []

elem_period = {}
elem_group = {}

current_period = 1
current_group = 1
def assign_period_group(elem):
    global valid_elems,elem_period,elem_group,current_period,current_group
    valid_elems.append(elem)
    elem_period[elem] = current_period
    elem_group[elem] = current_group

    if current_period==1 and current_group==1:
        if elem=="T":
            current_group = 32
    elif (current_period in [2,3]) and current_group==2:
        current_group = 27
    elif (current_period in [4,5]) and current_group==2:
        current_group = 17
    elif current_group==32:
        current_group = 1
        current_period +=1
    else:
        current_group += 1

# row 1
assign_period_group("H")
assign_period_group("D")
assign_period_group("T")
# for i in range(14):
#     assign_period_group("")
assign_period_group("He")

# row 2
assign_period_group("Li")
assign_period_group("Be")
# for i in range(10):
#     assign_period_group("")
assign_period_group("B")
assign_period_group("C")
assign_period_group("N")
assign_period_group("O")
assign_period_group("F")
assign_period_group("Ne")

# row 3
assign_period_group("Na")
assign_period_group("Mg")
# for i in range(10):
#     assign_period_group("")
assign_period_group("Al")
assign_period_group("Si")
assign_period_group("P")
assign_period_group("S")
assign_period_group("Cl")
assign_period_group("Ar")

# row 4
assign_period_group("K")
assign_period_group("Ca")
assign_period_group("Sc")
assign_period_group("Ti")
assign_period_group("V")
assign_period_group("Cr")
assign_period_group("Mn")
assign_period_group("Fe")
assign_period_group("Co")
assign_period_group("Ni")
assign_period_group("Cu")
assign_period_group("Zn")
assign_period_group("Ga")
assign_period_group("Ge")
assign_period_group("As")
assign_period_group("Se")
assign_period_group("Br")
assign_period_group("Kr")

# row 5
assign_period_group("Rb")
assign_period_group("Sr")
assign_period_group("Y")
assign_period_group("Zr")
assign_period_group("Nb")
assign_period_group("Mo")
assign_period_group("Tc")
assign_period_group("Ru")
assign_period_group("Rh")
assign_period_group("Pd")
assign_period_group("Ag")
assign_period_group("Cd")
assign_period_group("In")
assign_period_group("Sn")
assign_period_group("Sb")
assign_period_group("Te")
assign_period_group("I")
assign_period_group("Xe")

# row 6
assign_period_group("Cs")
assign_period_group("Ba")
# assign_period_group("Ln")

# row 6-Ln
# assign_period_group("")
# assign_period_group("Ln")
# assign_period_group("->")
assign_period_group("La")
assign_period_group("Ce")
assign_period_group("Pr")
assign_period_group("Nd")
assign_period_group("Pm")
assign_period_group("Sm")
assign_period_group("Eu")
assign_period_group("Gd")
assign_period_group("Tb")
assign_period_group("Dy")
assign_period_group("Ho")
assign_period_group("Er")
assign_period_group("Tm")
assign_period_group("Yb")
assign_period_group("Lu")

assign_period_group("Hf")
assign_period_group("Ta")
assign_period_group("W")
assign_period_group("Re")
assign_period_group("Os")
assign_period_group("Ir")
assign_period_group("Pt")
assign_period_group("Au")
assign_period_group("Hg")
assign_period_group("Tl")
assign_period_group("Pb")
assign_period_group("Bi")
assign_period_group("Po")
assign_period_group("At")
assign_period_group("Rn")

# row 7
assign_period_group("Fr")
assign_period_group("Ra")
# assign_period_group("An")

# row 7-An
# assign_period_group("")
# assign_period_group("An")
# assign_period_group("->")
assign_period_group("Ac")
assign_period_group("Th")
assign_period_group("Pa")
assign_period_group("U")
assign_period_group("Np")
assign_period_group("Pu")
assign_period_group("Am")
assign_period_group("Cm")
assign_period_group("Bk")
assign_period_group("Cf")
assign_period_group("Es")
assign_period_group("Fm")
assign_period_group("Md")
assign_period_group("No")
assign_period_group("Lr")

assign_period_group("Rf")
assign_period_group("Db")
assign_period_group("Sg")
assign_period_group("Bh")
assign_period_group("Hs")
assign_period_group("Mt")
assign_period_group("Ds")
assign_period_group("Rg")
assign_period_group("Cn")
assign_period_group("Nh")
assign_period_group("Fl")
assign_period_group("Mc")
assign_period_group("Lv")
assign_period_group("Ts")
assign_period_group("Og")

